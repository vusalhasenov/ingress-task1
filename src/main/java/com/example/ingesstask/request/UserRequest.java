package com.example.ingesstask.request;

import jakarta.validation.constraints.NegativeOrZero;
import jakarta.validation.constraints.NotBlank;
import lombok.AccessLevel;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.experimental.FieldDefaults;

@AllArgsConstructor
@NoArgsConstructor
@Data
@FieldDefaults(level = AccessLevel.PRIVATE)
public class UserRequest {
    @NotBlank(message = "username can't be null")
    String username;
    @NotBlank(message = "name can't be null")
    String name;
    @NotBlank(message = "surname can't be null")
    String surname;
    @NotBlank(message = "age can't be null")
    @NegativeOrZero(message = "age not negative and ziro")
    Integer age;
    @NotBlank(message = "balance can't be null")
    Double balance;
    Integer isActive;
}

package com.example.ingesstask.request;

import jakarta.validation.constraints.NegativeOrZero;
import jakarta.validation.constraints.NotBlank;
import lombok.AccessLevel;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.experimental.FieldDefaults;

@AllArgsConstructor
@NoArgsConstructor
@Data
@FieldDefaults(level = AccessLevel.PRIVATE)
public class ProductRequest {
    @NotBlank(message = "name not null")
    String name;
    @NotBlank(message = "price not null")
    @NegativeOrZero(message = "price not negative and zero")
    Double price;
    @NotBlank(message = "name not null")
    @NegativeOrZero(message = "stock count not negative and zero")
    Integer stockCount;
}

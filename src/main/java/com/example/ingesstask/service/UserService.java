package com.example.ingesstask.service;

import com.example.ingesstask.dto.UserDto;
import com.example.ingesstask.request.UserRequest;
import org.springframework.data.jpa.repository.Query;

import java.util.List;

public interface UserService {

    List<UserDto> all();

    UserDto findById(Long id);

    UserDto findByUserName(String username);

    Double findBalanceByUserName(String username);


    void save(UserRequest userRequest);

    void update(Long id,UserRequest userRequest);

    List<UserDto> findActiveUserList();
}

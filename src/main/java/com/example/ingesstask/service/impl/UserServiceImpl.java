package com.example.ingesstask.service.impl;

import com.example.ingesstask.dto.UserDto;
import com.example.ingesstask.entity.User;
import com.example.ingesstask.mapper.UserMapper;
import com.example.ingesstask.repository.UserRepository;
import com.example.ingesstask.request.UserRequest;
import com.example.ingesstask.service.UserService;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.stream.Collectors;

@Service
@RequiredArgsConstructor
public class UserServiceImpl implements UserService {

    private final UserRepository userRepository;
    private final UserMapper userMapper;

    @Override
    public List<UserDto> all() {
        return userRepository.findAll().stream().map(user -> userMapper.mapToDto(user)).collect(Collectors.toList());
    }

    @Override
    public UserDto findById(Long id) {
        return userRepository.findById(id).map(userMapper::mapToDto).orElseThrow(NullPointerException::new);
    }

    @Override
    public UserDto findByUserName(String username) {
        return userRepository.findByUsername(username).map(userMapper::mapToDto).orElseThrow(NullPointerException::new);
    }

    @Override
    public Double findBalanceByUserName(String username) {
        return userRepository.findBalanceByUsername(username).map(userMapper::mapToDto).orElseThrow(NullPointerException::new).getBalance();
    }

    @Override
    public void save(UserRequest userRequest) {
        userRepository.save(userMapper.mapToEntity(userRequest));
    }

    @Override
    public void update(Long id, UserRequest userRequest) {
        userRepository.findById(id).orElseThrow(NoSuchFieldError::new);
        User user = userMapper.mapToEntity(userRequest);
        user.setId(id);
        userRepository.save(user);
    }

    @Override
    public List<UserDto> findActiveUserList() {
        return userRepository.findActiveUser().stream().map(userMapper::mapToDto).collect(Collectors.toList());
    }
}

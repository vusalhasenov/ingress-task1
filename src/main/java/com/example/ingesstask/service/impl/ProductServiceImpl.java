package com.example.ingesstask.service.impl;

import com.example.ingesstask.dto.ProductDto;
import com.example.ingesstask.entity.Product;
import com.example.ingesstask.entity.User;
import com.example.ingesstask.exception.NotFoundException;
import com.example.ingesstask.mapper.ProductMapper;
import com.example.ingesstask.repository.ProductRepository;
import com.example.ingesstask.repository.UserRepository;
import com.example.ingesstask.request.ProductOrderRequest;
import com.example.ingesstask.request.ProductRequest;
import com.example.ingesstask.service.ProductService;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

@Service
@RequiredArgsConstructor
public class ProductServiceImpl implements ProductService {

    private final ProductRepository productRepository;
    private final ProductMapper productMapper;
    private final UserRepository userRepository;

    @Override
    public List<ProductDto> findAll() {
        return productRepository.findAll().stream().map(productMapper::mapToDto).collect(Collectors.toList());
    }

    @Override
    public ProductDto findById(Long id) {
        return productRepository.findById(id).map(productMapper::mapToDto).orElseThrow(NullPointerException::new);
    }

    @Override
    public void delete(Long id) {
        productRepository.findById(id).orElseThrow(NullPointerException::new);
        productRepository.deleteById(id);
    }

    @Override
    public void update(Long id ,ProductRequest productRequest) {
        productRepository.findById(id).orElseThrow(NullPointerException::new);
        Product product = productMapper.mapToEntity(productRequest);
        product.setId(id);
        productRepository.save(product);

    }

    @Override
    @Transactional
    public void order(String username, ProductOrderRequest productOrderRequest) {
        User byUsername = userRepository.findByUsername(username).orElseThrow(() -> new NotFoundException("username not found"));
        Product byId = productRepository.findById(productOrderRequest.getId()).orElseThrow(() -> new NotFoundException("product not found"));
        Double totalPrice = byId.getPrice()*productOrderRequest.getCount();
        if(byId.getStockCount()<productOrderRequest.getCount()){
            throw new NotFoundException("product not in stock");
        }
        if(byUsername.getBalance()<totalPrice){
            throw new NotFoundException("you don't have enough money");
        }
        Long updateStock = byId.getStockCount()-productOrderRequest.getCount();
        Double updateBalance = byUsername.getBalance()- totalPrice;
        byId.setStockCount(Math.toIntExact(updateStock));
        productRepository.save(byId);
        byUsername.setBalance(updateBalance);
        userRepository.save(byUsername);
    }
}

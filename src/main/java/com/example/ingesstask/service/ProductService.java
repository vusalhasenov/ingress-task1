package com.example.ingesstask.service;

import com.example.ingesstask.dto.ProductDto;
import com.example.ingesstask.request.ProductOrderRequest;
import com.example.ingesstask.request.ProductRequest;

import java.util.List;

public interface ProductService {

    List<ProductDto> findAll();

    ProductDto findById(Long id);

    void delete(Long id);

    void update(Long id ,ProductRequest productRequest);

    void order(String username , ProductOrderRequest productOrderRequest);
}

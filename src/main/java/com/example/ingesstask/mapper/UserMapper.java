package com.example.ingesstask.mapper;

import com.example.ingesstask.dto.UserDto;
import com.example.ingesstask.entity.User;
import com.example.ingesstask.request.UserRequest;
import org.mapstruct.Mapper;
import org.mapstruct.ReportingPolicy;

@Mapper(unmappedTargetPolicy = ReportingPolicy.IGNORE, componentModel = "spring")
public interface UserMapper {

    UserDto mapToDto(User user);

    User mapToEntity(UserRequest userRequest);

}

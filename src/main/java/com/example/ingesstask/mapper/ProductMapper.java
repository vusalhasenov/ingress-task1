package com.example.ingesstask.mapper;

import com.example.ingesstask.dto.ProductDto;
import com.example.ingesstask.entity.Product;
import com.example.ingesstask.request.ProductRequest;
import org.mapstruct.Mapper;
import org.mapstruct.ReportingPolicy;

@Mapper(unmappedTargetPolicy = ReportingPolicy.IGNORE, componentModel = "spring")
public interface ProductMapper {

    ProductDto mapToDto(Product product);

    Product mapToEntity(ProductRequest productRequest);
}

package com.example.ingesstask;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class IngessTaskApplication {

    public static void main(String[] args) {
        SpringApplication.run(IngessTaskApplication.class, args);
    }

}

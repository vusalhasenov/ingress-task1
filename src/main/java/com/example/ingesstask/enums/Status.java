package com.example.ingesstask.enums;

import lombok.AllArgsConstructor;
import lombok.Getter;
import org.springframework.web.bind.annotation.GetMapping;

import java.util.HashMap;
import java.util.Map;
import java.util.Optional;

@Getter
@AllArgsConstructor
public enum Status {

    ENABLED(1),
    DISABLE(2);


    private static final Map<Integer, Status> VALUE_MAP = new HashMap<>();

    static {
        for (Status type : values()) {
            VALUE_MAP.put(type.value, type);
        }
    }

    private final int value;

    public static Status getStatus(Integer value) {
        return VALUE_MAP.get(value);
    }

    public static Optional<Status> getStatusEnum(Integer value) {
        return Optional.ofNullable(VALUE_MAP.get(value));
    }



}

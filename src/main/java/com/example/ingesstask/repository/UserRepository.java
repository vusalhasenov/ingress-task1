package com.example.ingesstask.repository;

import com.example.ingesstask.entity.User;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import java.util.Optional;

public interface UserRepository extends JpaRepository<User,Long> {

    Optional<User> findByUsername(String username);

    Optional<User> findBalanceByUsername(String username);


    @Query(value = "select U from User U where U.isActive=?1")
    Optional<User> findActiveUser();

}

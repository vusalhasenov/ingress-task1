package com.example.ingesstask.repository;

import com.example.ingesstask.entity.Product;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

public interface ProductRepository extends JpaRepository<Product,Long> {

    @Query("update Product P set P.stockCount=P.stockCount-?1")
    void updateStock(Long count);
}

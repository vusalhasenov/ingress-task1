package com.example.ingesstask.controller;

import com.example.ingesstask.request.ProductOrderRequest;
import com.example.ingesstask.service.ProductService;
import jakarta.validation.Valid;
import lombok.RequiredArgsConstructor;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("api/product")
@RequiredArgsConstructor
public class ProductController {

    private final ProductService productService;

    @PostMapping("/{username}")
    public String order(@PathVariable("username") String username , @RequestBody @Validated ProductOrderRequest productOrderRequest){
        productService.order(username,productOrderRequest);
        return "order successfully";
    }
}

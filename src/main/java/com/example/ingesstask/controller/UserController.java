package com.example.ingesstask.controller;

import com.example.ingesstask.dto.UserDto;
import com.example.ingesstask.request.UserRequest;
import com.example.ingesstask.service.UserService;
import lombok.RequiredArgsConstructor;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("api/user")
@RequiredArgsConstructor
public class UserController {

    private final UserService userService;

    @GetMapping
    public ResponseEntity<List<UserDto>> all(){
        return ResponseEntity.ok(userService.all());
    }

    @GetMapping("/{id}")
    public ResponseEntity<UserDto> findById(@RequestParam("id") Long id){
        return ResponseEntity.ok(userService.findById(id));
    }

    @GetMapping("/get-username")
    public ResponseEntity<UserDto> findByUserName(@PathVariable String username){
        return ResponseEntity.ok(userService.findByUserName(username));
    }

    @GetMapping("/balance-username/{username}")
    public Double findBalanceByUserName(@PathVariable String username){
        return userService.findBalanceByUserName(username);
    }

    @PutMapping("/update/{id}")
    public String update(@PathVariable Long id,@RequestBody UserRequest userRequest){
        userService.update(id,userRequest);
        return "update success";
    }

    @PostMapping
    public String save(@RequestBody UserRequest userRequest){
        userService.save(userRequest);
        return "save successfully";
    }

    @GetMapping("/active-users")
    public ResponseEntity<List<UserDto>> findActiveUsers(){
        return ResponseEntity.ok(userService.findActiveUserList());
    }
}
